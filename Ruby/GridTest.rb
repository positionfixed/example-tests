require 'selenium-webdriver'
require 'test/unit'
require 'ci/reporter/rake/test_unit_loader'
 
class GridTest < Test::Unit::TestCase
 
    def setup
        gridUrl = "http://localhost:4444/wd/hub";
   
        # You can select another browser by changing the Capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.firefox();
           
        # Launches a new Firefox instance on the TestGrid server
        @driver = Selenium::WebDriver.for(:remote, :url =>gridUrl, :desired_capabilities => capabilities)
    end
 
    def testGrid
        @driver.navigate.to 'http://www.selenium.academy/Examples/Waiting.html'
        
        # Create a new WebDriverWait object that waits up to 5 seconds
        wait = Selenium::WebDriver::Wait.new(:timeout => 5)

        # Instruct Selenium to wait until it can find the element
        button = wait.until { @driver.find_element(:id => "button") }
        button.click

    end
 
    def teardown
        @driver.quit
    end
end