require 'ci/reporter/rake/test_unit'

task :test => 'ci:setup:testunit'

task default: %w[test]

task :test do
  ruby "GridTest.rb"
end