import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
 
class TestGrid(unittest.TestCase):
 
    def setUp(self):
        gridUrl = 'http://localhost:4444/wd/hub'
        
        # You can select another browser by changing the DesiredCapabilities
        capabilities = webdriver.DesiredCapabilities.FIREFOX
        
        # Launches a new Firefox instance on the TestGrid server
        self.driver = webdriver.Remote(command_executor = gridUrl, desired_capabilities = capabilities)
 
    def tearDown(self):
        # close the Chrome instance
        self.driver.quit()
 
    def testGrid(self):
        self.driver.get('http://www.selenium.academy/Examples/Waiting.html')

        # Instruct Selenium to wait up to 5 seconds
        # Until the element is available
        button = WebDriverWait(self.driver, 5).until(
            EC.presence_of_element_located((By.ID, 'button'))
        )
 
        button.click
 
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        import xmlrunner
        runner = xmlrunner.XMLTestRunner(output='test-reports')

    unittest.main(testRunner=runner)
