
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GridTest {
    @Test
    public void testGrid() throws MalformedURLException {  
        
        URL gridUrl = new URL("http://localhost:4444/wd/hub");

        // You can select another browser by changing the options
        Capabilities capabilities = new FirefoxOptions();

        // Launches a new Firefox instance on the TestGrid server
        WebDriver driver = new RemoteWebDriver(gridUrl, capabilities);
        
        driver.navigate().to("http://www.selenium.academy/Examples/Waiting.html");
        
        // Create a new WebDriver wait object that will wait up to 5 seconds
        WebDriverWait wait = new WebDriverWait(driver, 5);
        
        // Instruct Selenium to wait until the element is clickable
        wait.until(ExpectedConditions.elementToBeClickable(By.id("button")));
        
        WebElement button = driver.findElement(By.id("button"));
        
        button.click();
        
        driver.quit();
    }
}
