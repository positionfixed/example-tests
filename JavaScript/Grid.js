var assert = require('assert'),
webdriver = require('selenium-webdriver');
  
describe('Selenium Academy', async function() {
  it('should work', async function() {
 
    this.timeout(15000);
 
    var gridUrl = 'http://localhost:4444/wd/hub';
     
    // You can select another browser by changing the DesiredCapabilities
    var capabilities = webdriver.Capabilities.firefox();
         
    // Launches a new Firefox instance on the TestGrid server
    var driver = new webdriver.Builder().withCapabilities(capabilities)
                                        .usingServer(gridUrl)
                                        .build();

    await driver.get('http://www.selenium.academy/Examples/Waiting.html');                                        

    // Instruct Selenium to wait up to 5 seconds for the element
    var button = await driver.wait(webdriver.until.elementLocated(webdriver.By.id('button')), 5000);
    await button.click();

    await driver.quit();
 
  });
});