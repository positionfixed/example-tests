﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace Project
{
    [TestClass]
    public class Clicking
    {
        [TestMethod]
        public void TestClick()
        {
            Uri gridUrl = new Uri("http://localhost:4444/wd/hub");

            // You can select another browser by changing the DesiredCapabilities
            ICapabilities capabilities = new FirefoxOptions().ToCapabilities();

            // Launches a new Firefox instance on the TestGrid server
            IWebDriver driver = new RemoteWebDriver(gridUrl, capabilities);

            driver.Navigate().GoToUrl("http://www.selenium.academy/Examples/Waiting.html");

            // Create a new WebDriverWait object that will wait up to 5 seconds
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));

            // Instruct Selenium to wait until the element is clickable
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("button")));

            IWebElement button = driver.FindElement(By.Id("button"));

            button.Click();

            driver.Quit();
        }
    }
}
